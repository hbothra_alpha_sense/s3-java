package example;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.S3Event;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LogHandler implements RequestHandler<S3Event, String> {

    Gson gson = new GsonBuilder().setPrettyPrinting()
                                 .create();
    private static final Logger logger = LoggerFactory.getLogger(LogHandler.class);

    @Override
    public String handleRequest(S3Event s3Event, Context context) {
        logger.info("EVENT: " + gson.toJson(s3Event));
        return "Ok";
    }
}
