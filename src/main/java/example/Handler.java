package example;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.S3Event;
import com.amazonaws.services.lambda.runtime.events.models.s3.S3EventNotification;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.DeleteObjectsRequest;
import com.amazonaws.services.s3.model.DeleteObjectsResult;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.S3Object;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

// Handler value: example.Handler
public class Handler implements RequestHandler<S3Event, String> {

    Gson gson = new GsonBuilder().setPrettyPrinting()
                                 .create();
    private static final Logger logger = LoggerFactory.getLogger(Handler.class);
    // Use File separator else it will fail in Linux box
    private final String fileTmpDir = System.getProperty("java.io.tmpdir") + File.separator;

    @Override
    public String handleRequest(S3Event s3event, Context context) {

        logger.info("EVENT: " + gson.toJson(s3event));
        S3EventNotification.S3EventNotificationRecord record = s3event.getRecords()
                                                                      .get(0);

        String srcBucket = record.getS3()
                                 .getBucket()
                                 .getName();

        // Object key may have spaces or unicode non-ASCII characters.
        String srcKey = record.getS3()
                              .getObject()
                              .getUrlDecodedKey();

        String folder = srcKey.split("[/]")[0];
        String fileName = srcKey.split("[/]")[1].split("[.]")[0];

        String zipKey = fileTmpDir + fileName + ".zip";

        String dstBucket = srcBucket; // This has to be changes to direct feed
        String dstKey = "Destination/" + folder + "/" + fileName + ".zip";

        List<DeleteObjectsRequest.KeyVersion> keys = new ArrayList<>();
        keys.add(new DeleteObjectsRequest.KeyVersion(srcKey));
        keys.add(new DeleteObjectsRequest.KeyVersion(srcKey.replace(".xml", ".pdf")));

        // Download the image from S3 into a stream
        AmazonS3 s3Client = AmazonS3ClientBuilder.defaultClient();
        S3Object metaObject = s3Client.getObject(new GetObjectRequest(
                        srcBucket, srcKey));
        // Can add logic to read xml to get pdf name from the same
        S3Object dataObject = s3Client.getObject(new GetObjectRequest(
                        srcBucket, srcKey.replace(".xml", ".pdf")));
        try {
            FileOutputStream fos = new FileOutputStream(zipKey);
            ZipOutputStream zipOut = new ZipOutputStream(fos);
            addFiletoZip(srcKey, metaObject, zipOut);
            addFiletoZip(srcKey.replace(".xml", ".pdf"), dataObject, zipOut);
            zipOut.close();
            fos.close();
            logger.info("Zipped successfully to: " + zipKey);
            ObjectMetadata meta = new ObjectMetadata();
            meta.setContentType("application/zip");
            // Uploading to S3 destination bucket
            logger.info("Writing to: " + dstBucket + "/" + dstKey);
            try {
                File zip = new File(zipKey);
                meta.setContentLength(zip.length());
                s3Client.putObject(dstBucket, dstKey, new FileInputStream(new File(zipKey)), meta);
                zip.delete();
            } catch (AmazonServiceException e) {
                logger.error(e.getErrorMessage());
                System.exit(1);
            }
            logger.info("Successfully zipped " + srcBucket + "/"
                        + srcKey + " and uploaded to " + dstBucket + "/" + dstKey);
            DeleteObjectsResult delObjRes = s3Client.deleteObjects(new DeleteObjectsRequest(srcBucket)
                                                                                   .withKeys(keys)
                                                                                   .withQuiet(false));
            logger.info("Successfully deleted {} files from {}",
                        delObjRes.getDeletedObjects()
                                 .size(),
                        srcBucket);
            return "Ok";
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void addFiletoZip(String srcKey, S3Object object, ZipOutputStream zipOut) throws IOException {
        ZipEntry zipEntry = new ZipEntry(srcKey);
        zipOut.putNextEntry(zipEntry);
        byte[] bytes = new byte[1024];
        int length;
        while ((length = object.getObjectContent()
                               .read(bytes)) >= 0) {
            zipOut.write(bytes, 0, length);
        }
    }
}