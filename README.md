# S3 Zip and Upload (Java)

The project source includes function code and supporting resources:

- `src/main` - A Java function.
- `src/test` - A unit test and helper classes.
- `pom.xml` - A Maven build file.

Use the following instructions to deploy the sample application.

# Requirements
- [Java 8 runtime environment (SE JRE)](https://www.oracle.com/java/technologies/javase-downloads.html)
- [Maven 3](https://maven.apache.org/docs/history.html)
- [The AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/cli-chap-install.html) v1.17 or newer.

This application is used in https://us-east-1.console.aws.amazon.com/lambda/home?region=us-east-1#/functions/S3ziptoS3?tab=monitoring

This application is listening to s3 event register with lambda (we are expecting those to be only .xml file events)
Once event is received it will extract bucket, folder and file information based on same will fetch pdf file with
same name of xml and create a zip file and place the same in `/tmp` folder from where the same zip file is pushed
to destination folder of same bucket inside which it follows same naming as of xml replaced by .zip

For further questions reach out hbothr@alpha-sense.com

